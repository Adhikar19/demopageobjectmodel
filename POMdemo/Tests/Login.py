from selenium import  webdriver
import time
import unittest

class LoginTest(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="D:\Selinium\chromedriver_win32\chromedriver.exe")
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()


    def testLogin(self):
        self.driver.get("https://demo.opencart.com/admin/")
        self.driver.find_element_by_id("input-username").send_keys("demo")
        self.driver.find_element_by_id("input-username").clear()
        self.driver.find_element_by_id("input-password").send_keys("demo")
        self.driver.find_element_by_id("input-password").clear()
        self.driver.find_element_by_xpath("//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button").click()
        time.sleep(5)

    @classmethod
    def tearDownClass(cls):
        cls.close()
        cls.driver.quit()
        print("Test completed")

    if __name__ == '__main__':
        unittest.main()

