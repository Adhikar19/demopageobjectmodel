import unittest
from selenium import webdriver
import time
from selenium.common.exceptions import TimeoutException, ElementNotInteractableException, NoSuchElementException


class testCategory(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome("D:\Selinium\chromedriver_win32\chromedriver.exe")
        self.driver.maximize_window()
        self.driver.get("https://demo.opencart.com/admin/")
        username = self.driver.find_element_by_id("input-username")
        username.clear()
        username.send_keys("demo")
        password = self.driver.find_element_by_id("input-password")
        password.clear()
        password.send_keys("demo")

        login = self.driver.find_element_by_xpath("//*[@id='content']/div/div/div/div/div[2]/form/div[3]/button")
        login.click()
        time.sleep(5)


    def testcatelogadd(self):
        try:
            catalog =self.driver.find_element_by_xpath("//*[@id='menu-catalog']/a")
            catalog.click()
            catalogmodule = self.driver.find_element_by_xpath("//*[@id='collapse1']/li[1]/a")
            catalogmodule.click()
            addclick = self.driver.find_element_by_xpath("//*[@id='content']/div[1]/div/div/a[1]")
            addclick.click()
            add_category = self.driver.find_element_by_id("input-name1")
            add_category.clear()
            add_category.send_keys("Speaker")
            description = self.driver.find_element_by_xpath("//*[@id='language1']/div[2]/div/div/div[3]/div[2]")
            description.clear()
            description.send_keys("This is the high quality speaker")
            metatag = self.driver.find_element_by_id("input-meta-title1")
            metatag.clear()
            metatag.send_keys("This is meta tag type")

            Meta_Tag_Description = self.driver.find_element_by_id("input-meta-description1")
            Meta_Tag_Description.clear()
            Meta_Tag_Description.send_keys("This is meta tag description ")

            Meta_Tag_Keywords = self.driver.find_element_by_id("input-meta-keyword1")
            Meta_Tag_Keywords.clear()
            Meta_Tag_Keywords.send_keys("THis is the meta tag keywords")

            Save = self.find_element_by_xpath("//*[@id='content']/div[1]/div/div/button")
            Save.click()
            time.sleep(8)


        except ElementNotInteractableException:
            print("NO such exception")



if __name__ == '__main__':
    unittest.main()




